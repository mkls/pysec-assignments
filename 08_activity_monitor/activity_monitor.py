"""
Find all packets sent outside of a specified
time range ('work hours')
"""

import datetime as dt
from argparse import ArgumentParser
from scapy.all import *
from typing import Any, Dict, Iterable


def read_layers(packet: Packet) -> Dict[str, Dict[str, Any]]:
    """
    Reads packet layers, layer fields and stores them
    in a dictionary {layer: {field: value}}
    """
    fields = {}

    for layer in packet.layers():
        fields[layer] = {}

        for field in packet[layer].fields:
            fields[layer][field] = getattr(packet[layer], field)

    return fields


def recursive_print(d: dict, left_padding: int = 15, level: int = 0) -> None:
    """
    Pretty print d recursively

    :param: d - dictionary-like object to print
    :param: left_padding - value to use to pad inner values (default is 15)
    :param: level - indentation level to start with (default is 0)
    :returns: None
    """
    for k, v in d.items():
        print(f'{k}:'.rjust(left_padding * level))

        if type(v) == dict:
            recursive_print(v, left_padding, level + 1)
        elif type(v) == list:
            print('['.rjust(left_padding * (level + 1)), end='')
            print(*v, end='')
            print(']')
        else:
            print(str(v).rjust(left_padding * (level + 1)))


def find_outliers(start_t: dt.time, end_t: dt.time,
                  packets: List[Packet]) -> Iterable[Packet]:
    """
    Find packets outside of a specified time range

    :param: start_t - start of the time range (in UTC/ISO)
    :param: end_t - start of the time range (in UTC/ISO)

    :returns: a generator of filtered packets
    """
    for packet in packets:
        packet_t = dt.datetime.fromtimestamp(int(packet.time)).time()

        if start_t > end_t:
            # E.g. permitted range is from 23:00 to 01:00
            # Check with shifted bounds
            start_tt = start_t.replace(hour=0)
            end_tt = end_t.replace(hour=end_t.hour + (24 - start_t.hour))
            packet_tt = packet_t.replace(hour=packet_t.hour + (24 - start_t.hour))

            if not (start_tt <= packet_tt <= end_tt):
                yield packet

        elif not (start_t <= packet_t <= end_t):
            yield packet


if __name__ == '__main__':
    parser = ArgumentParser('Packet outlier detection')
    parser.add_argument('pcap', help='Path to pcap file')
    parser.add_argument('lower',
                        help='Lower bound of a permitted time range (UTC)')
    parser.add_argument('upper',
                        help='Upper bound of a permitted time range (UTC)')
    parser.add_argument('-v', help='Print every outlier packet\' details',
                        dest='v', action='store_true')
    parser.set_defaults(v=False)

    args = parser.parse_args()

    packets = rdpcap(args.pcap)
    start_dt = dt.time.fromisoformat(args.lower)
    end_dt = dt.time.fromisoformat(args.upper)

    outliers = find_outliers(start_dt, end_dt, packets)

    count = 0
    for i, p in enumerate(outliers):
        if args.v:
            print(f'Packet {i}'.rjust(10)) 
            print('-' * 34)
            recursive_print(read_layers(p))
        count += 1

    print(f'Found {count} outliers')

