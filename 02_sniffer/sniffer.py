from functools import partial
from scapy.arch import get_if_addr
from scapy.layers.inet import TCP
from scapy.packet import Packet
from scapy.sendrecv import sniff
from scapy.utils import wrpcap
from typing import List


def packet_prn(x: Packet, target_addr: str, ports: List[int]) -> None:
    sport = x['TCP'].sport
    dport = x['TCP'].dport

    src_addr = x['IP'].src
    dst_addr = x['IP'].dst

    src = f'{src_addr}:{sport}'
    dst = f'{dst_addr}:{dport}'

    if x[TCP].flags == 'S':
        if sport in ports:
            wrpcap(f'{sport}.pcap', x, append=True)
        elif dport in ports:
            wrpcap(f'{dport}.pcap', x, append=True)

def sniff_packets(iface: str, ports: List[int]) -> None:
    filt = 'tcp and (' + ' or '.join(map(lambda p: f'port {p}', ports)) + ')'
    target_addr = get_if_addr(iface)
    prn = partial(packet_prn, target_addr=target_addr, ports=ports)
    sniff(iface=iface, prn=prn, filter=filt)

sniff_packets('enp8s0', [22, 80, 443])

