from scapy.all import *
from typing import Iterable, Set

def get_port(packet: Packet, ports: Set[int], layer: Packet) -> int:
    dport = packet[layer].dport
    sport = packet[layer].sport

    if sport in ports:
        t_port = sport
    elif dport in ports:
        t_port = dport
    else:
        t_port = None

    return t_port

def calculate_traffic_size(packets: Iterable[Packet], ports: Set[int]) -> int:
    """
    Calculates size of all packets in bytes

    :param: packets - iterable sequence of scapy packets
    :param: ports - list of ports to count independently
    :returns: total number of bytes sent
    """
    results = {
        'arp': 0,
        'others': 0
    }
    
    for port in ports:
        results[port] = 0

    for packet in packets:
        t_port = None

        if TCP in packet.layers():
            t_port = get_port(packet, ports, TCP)
        elif UDP in packet.layers():
            t_port = get_port(packet, ports, UDP)
        elif ARP in packet.layers():
            t_port = 'arp'

        if t_port is None:
            t_port = 'others'

        results[t_port] += len(packet)
    
    return results

if __name__ == '__main__':
    packet = rdpcap('task3.pcap')
    x = calculate_traffic_size(packet, {22, 80, 443})
    print('-' * 23, '\n', 'Result'.rjust(13))
    print(x)

