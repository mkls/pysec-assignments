class DiffieHellman:
    def __init__(self, public_key1: int, public_key2: int, private_key: int):
        self.pubkey1 = public_key1
        self.pubkey2 = public_key2
        self.private_key = private_key

    def gen_partial_key(self):
        partial_key = (self.pubkey1 ** self.private_key) % self.pubkey2
        self.partial_key = partial_key

    def gen_full_key(self, partial_other: int):
        full_key = (partial_other ** self.private_key) % self.pubkey2
        self.full_key = full_key

    def encrypt(self, msg: str):
        encrypted = ''.join(map(lambda x: chr(ord(x) + self.full_key), msg))
        return encrypted

    def decrypt(self, msg: str):
        decrypted = ''.join(map(lambda x: chr(ord(x) - self.full_key), msg))
        return decrypted


if __name__ == '__main__':
    m1_pub = 123
    m2_pub = 443

    m1_priv = 80
    m2_priv = 59

    d1 = DiffieHellman(m1_pub, m2_pub, m1_priv)
    d2 = DiffieHellman(m1_pub, m2_pub, m2_priv)

    p1 = d1.gen_partial_key()
    p2 = d2.gen_partial_key()

    d1.gen_full_key(p2)
    d2.gen_full_key(p1)

    print(d1.full_key)
    print(d2.full_key)

    enc = d1.encrypt('Hello world!')
    print(enc)
    dec = d2.decrypt(enc)
    print(dec)

