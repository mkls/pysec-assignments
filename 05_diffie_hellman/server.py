import asyncio
import websockets
import json
from secrets import randbits
from diffie_hellman import DiffieHellman

dh = DiffieHellman(None, randbits(15), randbits(15))

async def handler(ws):
    async for message in ws:
        message = json.loads(message)
        if message['type'] == 'exchange':
            dh.pubkey1 = message['value']
            await ws.send(json.dumps(dh.pubkey2))
            dh.gen_partial_key()
        elif message['type'] == 'exchange_partial_keys':
            dh.gen_full_key(message['value'])
            await ws.send(json.dumps(dh.partial_key))
        elif message['type'] == 'send':
            msg = dh.decrypt(message['value'])
            print(msg)

async def main():
    async with websockets.serve(handler, "localhost", 8765):
        await asyncio.Future()

try:
    asyncio.run(main())
except KeyboardInterrupt:
    print('Exiting')

