import asyncio
import websockets
import json
from sys import argv
from secrets import randbits
from diffie_hellman import DiffieHellman

dh = DiffieHellman(randbits(10), None, randbits(10))

async def exchange_pubkeys():
    async with websockets.connect("ws://localhost:8765") as ws:
        await ws.send(json.dumps({'type': 'exchange', 'value': dh.pubkey1}))
        dh.pubkey2 = json.loads(await ws.recv())
        dh.gen_partial_key()

async def exchange_partial_keys():
    async with websockets.connect("ws://localhost:8765") as ws:
        await ws.send(json.dumps({'type': 'exchange_partial_keys',
                                  'value': dh.partial_key}))
        partial_key = json.loads(await ws.recv())
        dh.gen_full_key(partial_key)

async def send_encrypted_message(msg: str):
    encrypted = dh.encrypt(msg)

    async with websockets.connect("ws://localhost:8765") as ws:
        await ws.send(json.dumps({'type': 'send', 'value': encrypted}))

if __name__ == '__main__':
    msg = argv[1]
    asyncio.run(exchange_pubkeys())
    asyncio.run(exchange_partial_keys())
    asyncio.run(send_encrypted_message(msg))

