from scapy.all import *
from typing import Any, Dict, List


def read_layers(packet: Packet) -> Dict[str, Dict[str, Any]]:
    """
    Reads packet layers, layer fields and stores them
    in a dictionary {layer: {field: value}}
    """
    fields = {}

    for layer in packet.layers():
        fields[layer] = {}

        for field in packet[layer].fields:
            fields[layer][field] = getattr(packet[layer], field)

    return fields


def compare_packets(packet1: Packet, packet2: Packet) -> Dict[Any, Dict[str, Any]]:
    """
    Compare two scapy packets
    :param: packet1 - packet to compare against
    :param: packet2 - packet to compare with packet1

    :returns: List of unidentical layers with unique fields
    """
    packet1_d = read_layers(packet1)
    packet2_d = read_layers(packet2)

    difference = {}

    for layer in packet2_d.keys():
        difference[layer] = {}

        fields_2 = packet2_d[layer]

        if layer in packet1:
            fields_1 = packet1_d[layer]
        else:
            fields_1 = {}

        unique_1 = fields_1.keys() - fields_2.keys()
        unique_2 = fields_2.keys() - fields_1.keys()
        common = fields_1.keys() - unique_1

        for field in common:
            if packet2_d[layer][field] != packet1_d[layer][field]:
                difference[layer][field] = {
                    'packet_1': packet1_d[layer][field],
                    'packet_2': packet2_d[layer][field]
                }

        for field in unique_1:
            difference[layer][field] = {
                'packet_1': packet1_d[layer][field],
                'packet_2': None
            }

        for field in unique_2:
            difference[layer][field] = {
                'packet_1': None,
                'packet_2': packet2_d[layer][field]
            }

        if len(difference[layer]) == 0:
            difference.pop(layer)

    return difference


def compare_3(packet1: Packet, packet2: Packet,
              packet3: Packet) -> List[Dict[Any, Dict[str, Any]]]:
    """
    Compare 3 packets
    :param: packet1 - reference packet
    :param: packet2 - first packet to compare
    :param: packet3 - second packet to compare (also compared with packet2)
    :return: list of dictionaries with different per-packet layer fields
    """
    # Compare packet2 with packet1
    diff21 = compare_packets(packet1, packet2)
    # Compare packet3 with packet1
    diff31 = compare_packets(packet1, packet3)
    # Compare packet3 with packet2
    diff32 = compare_packets(packet2, packet3)

    return [diff21, diff31, diff32]


def recursive_print(d: dict, left_padding: int = 15, level: int = 0) -> None:
    """
    Pretty print d recursively

    :param: d - dictionary-like object to print
    :param: left_padding - value to use to pad inner values (default is 15)
    :param: level - indentation level to start with (default is 0)
    :returns: None
    """
    for k, v in d.items():
        print(f'{k}:'.rjust(left_padding * level))

        if type(v) == dict:
            recursive_print(v, left_padding, level + 1)
        elif type(v) == list:
            print('['.rjust(left_padding * (level + 1)), end='')
            print(*v, end='')
            print(']')
        else:
            print(str(v).rjust(left_padding * (level + 1)))


def main():
    # Read packet dump
    p_dump = rdpcap('443.pcap')

    p1 = p_dump[0]
    p2 = p_dump[1]
    p3 = p_dump[2]

    # diff = compare_packets(p1, p2)
    # recursive_print(diff)

    diff3 = compare_3(p1, p2, p3)
    for i, d in enumerate(diff3):
        print('Comparison', i + 1)
        recursive_print(d, 10)


if __name__ == "__main__":
    main()

