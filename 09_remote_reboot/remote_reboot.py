import time
from sys import stdin
from paramiko import Transport, PKey, Channel
from paramiko.ed25519key import Ed25519Key
from typing import Tuple


class RemoteCommandSender:
    """
    Wrapper over paramiko lower-level objects used to
    send command to remote server
    """
    def __init__(self, host: str, port: int, user: str,
                 password: str = None, private_key: PKey = None):
        """
        Construct new sender instance

        :param: host - host to connect to
        :param: port - target port
        :param: user - name of the user at the remote
        :param: password - password for authorization, set to None
            if you want to use ssh keys (default)
        :param: private_key - private_key to use for authorization,
            set to None if you want to use password-auth
        """

        if password is None and private_key is None:
            raise ValueError('Either password or private_key must be not None')

        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.private_key = private_key

    def __run_cmd__(self, cmd: str, session: Channel,
                    exit_session: bool = True,
                    termination_str: str = '$ ') -> Tuple[int, str, str]:
        """
        Simply execute single command and return exit_status, stdout and stderr

        :param: cmd - command to run, must end with a newline char
        :param: session - Channel object to use to execute the command
        :param: exit_session - if True, exits the session after completion

        :returns: exit status, stdout and stderr
        """
        print(f'Sending command "{cmd}" to execute')
        session.send(cmd + "\n")
        # stdin.write(cmd + "\n")
        stdout = ""
        stderr = ""

        print('Waiting for a second')
        time.sleep(1)

        while True:
            out = session.recv(1024).decode()
            stdout += out
            print(out, end='')

            if out.endswith(termination_str):
                break

        stdout += out
        # stderr += err

        if stderr == "":
            exit_status = 0
        else:
            exit_status = 1

        if exit_session:
            session.close()

        return exit_status, stdout, stderr

    def get_user_shell(self) -> str:
        """
        A shortcut function to get current user's shell

        :returns: output of echo $SHELL
        """
        self.client = Transport((self.host, self.port))
        self.client.connect(username=self.user, password=self.password,
                            pkey=self.private_key)
        session = self.client.open_channel(kind='session')
        session.exec_command('echo $SHELL\n')

        stdout = ""
        stderr = ""

        while not session.exit_status_ready():
            stdout += session.recv(1024).decode()
            stderr += session.recv_stderr(1024).decode()

        exit_status = session.recv_exit_status()
        session.close()

        if exit_status != 0:
            return stderr
        else:
            return stdout

    def exec_command(self, cmd: str, interactive: bool = False, *args, **kwargs) -> int:
        """
        Execute command on a remote server. Automatically starts
        and closes ssh session.

        :param: cmd - command to execute on the remote host
        :param: *args - additional arguments to pass to paramiko
        :param: **kwargs - additional kwargs to pass to paramiko

        :returns: exit status
        """
        self.client = Transport((self.host, self.port))
        self.client.connect(username=self.user, password=self.password,
                            pkey=self.private_key)

        session = self.client.open_channel(kind='session')

        if interactive:
            print('Starting interactive shell')

            # session.set_combine_stderr(True)
            session.get_pty()
            session.invoke_shell()

            while True:
                out = session.recv(1024).decode()

                if '$' in out or '#' in out:
                    break

            shell = self.get_user_shell()

            print(shell)

            # TODO: find proper way to wait until command actually completes
            # for now simply check what shell is running and override the prompt.
            # Works for POSIX-compatible shells (by overriding PS1) or by
            # setting fish_prompt in case of fish shell

            # It WILL BREAK if you try to ssh inside ssh or something like that.
            # Also it will break on any software that requires your input with
            # some alternative prompt
            print('Overriding default prompt')
            # session.send('echo $SHELL\n')
            status = 0

            if status == 0:
                if 'fish' in shell:
                    prompt = '''
                    function fish_prompt
                        printf "]]\$ "
                    end
                    '''
                else:
                    prompt = r'export PS1="]]\$ "'

                # Override default prompt
                status, stdout, stderr = self.__run_cmd__(prompt, session,
                                                          exit_session=False,
                                                          termination_str=r']]$ ')
                if status != 0:
                    print(stdout)
                    print(stderr)

                    print('Failed to start interactive session')
                    session.close()
                    self.client.close()

                    return 1

            else:
                session.close()
                self.client.close()

                return 1

            out = ""

            try:
                self.__run_cmd__(cmd, session, exit_session=False,
                                 termination_str='rd: ')
                while True:
                    user_input = input()
                    session.send(user_input + '\n')

                    if user_input == 'exit':
                        print('Log out')
                        exit_status = 0
                        break

                    out = ""
                    while True:
                        buf = session.recv(1024).decode()
                        print(buf, end='')
                        out += buf

                        if out.endswith(r']]$ ') or out.endswith('Password: '):
                            break
            except OSError:
                print('SSH connection terminated')
                exit_status = 1
            except (KeyboardInterrupt, EOFError):
                exit_status = 0
            finally:
                session.close()
                self.client.close()
        else:
            session.exec_command(cmd)

            stdout = ""
            stderr = ""

            while not session.exit_status_ready():
                stdout += session.recv(1024).decode()
                stderr += session.recv_stderr(1024).decode()

            exit_status = session.recv_exit_status()

            print('Command exited with status', exit_status)
            print(f'Stdout:\n{stdout}')
            print(f'Stderr:\n{stderr}')

            session.close()
            self.client.close()

        return exit_status


if __name__ == '__main__':
    pkey = Ed25519Key.from_private_key_file(filename='',
                                            password='')
    executor = RemoteCommandSender(
        host='',
        port=22,
        user='mkls',
        private_key=pkey
    )
    exit_status = executor.exec_command(cmd='systemctl reboot')

    if exit_status != 0:
        print("Failed to execute command. Retrying in interactive mode")
        executor.exec_command(cmd='systemctl reboot', interactive=True)

